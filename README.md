﻿# Windows essential & recommended programs

*Note:* also see [https://ninite.com/](https://ninite.com/)

------------

## Maintenance

- ??? **Microsoft Security Essentials** - antivirus
- **CCleaner** - cleaning tool (removes unused files, traces of online activity, registry cleaner)
- ??? **Defraggler** - defragmentation tool
- **Double Driver** - backup, restore, save and print all installed drivers
- **Autoruns for Windows** - find/disable/delete anything that starts up automatically
- **WinDirStat** - disk usage analyzer
- **Process Hacker** - an advanced task manager
- **CPU-Z** - see system information like processor and mainboard

## Internet

- **Mozilla Firefox** - web browser
- **Google Chrome** - web browser
- **uBlock Origin** - efficient blocker add-on for various browsers (blocks ads, trackers and malware sites)
- **qBittorrent** - BitTorrent client
- **Thunderbird** - email client
- **HexChat** - IRC client
- **PuTTY** - SSH client
- **FileZilla** - FTP client
- **WinSCP** - SFTP, SCP and FTP client
- **TunnelBear** - very simple VPN app (500 MB free per month)
- **Free Download Manager** - download online files, can pause and resume (broken/interrupted) downloads
- **Discord** - instant messaging and VoIP social platform

## Audio / Music

- **foobar2000** - music player
- **Audacity** - audio editor and recorder
- **Mp3tag** - (automatic) track/album tagging
- **Spotify** - music streaming service

## Video

- **VLC** - video player
- **HandBrake** - video transcoder (converting between video formats)
- **OpenShot** - relatively simple video editor

## Document viewing & creation

- **Sumatra PDF** -  PDF reader
- **Foxit Reader** - PDF reader and creator
- **Foxit PDF Editor** - edit PDF files
- **LibreOffice** - free alternative to MS Office (Word, PowerPoint, Excel etc.)
- **Notepad++** - Notepad replacement and source code editor
- **CDisplay Ex** - reads all comic book formats (.cbr, .cbz, .pdf, etc.)
- **MiKTeX** - TeX/LaTeX distribution
- **TeXstudio** - LaTeX editor
- **Texmaker** - LaTeX editor
- **Zotero** - easy-to-use tool to collect, organize, cite and share bibliographies/citations
- **Obsidian** - writing app based on Markdown
- **calibre** - e-book management

## Image viewing & manipulation

- **Adobe Photoshop** - graphics editing program
- **GIMP** - free alternative to Photoshop
- **Paint.NET** - free alternative to Photoshop
- **Photoscape** - view, optimize, resize, edit, print photos
- **Inkscape** - vector graphics editor
- **ImageMagick** - CLI tool for editing and manipulating images

## CD / DVD / ISO

- **ImgBurn** - lightweight CD/DVD/ISO burning software
- **AnyBurn** - lightweight CD/DVD/ISO burning software
- **WinCDEmu** - CD/DVD/BD emulator, used to mount optical disc images (.iso)
- **Rufus** - create bootable USB drives

## Programming & web development

- **Visual Studio Code** - code editor
- **Eclipse** - IDE
- **NetBeans** - IDE
- **Brackets** - code editor (primarily for web design)
- **Git** - distributed version control system
- **XAMPP** - installs Apache, PHP, MySQL and Perl on your PC
- **Gow** - lightweight alternative to Cygwin
- **PyCharm** - Python IDE
- **Anaconda** - Python distribution with IDE, includes popular (scientific) Python packages
- **Code::Blocks** - lightweight C, C++ and Fortran IDE (some download versions also include a compiler)
- **Postman** - GUI app for testing and sharing API requests

## Other

- **KeePassXC** - password manager
- **7-Zip** - utility for manipulating archives (.rar, .zip etc.)
- **f.lux** - makes the color of your computer's display adapt to the time of day
- **CareUEyes** - control the brightness on your screens/monitors via the keyboard (paid app)
- **Dropbox** - online storage/backup
- **TeamViewer** - remote access and control
- **AnyDesk** - remote access and control (alternative to TeamViewer)
- **Bulk Rename Utility** - easily rename files/folders based upon extremely flexible criteria
- **WinMerge** - differencing and merging tool, compares both folders and files
- **LICEcap** - simple animated (.gif) screen captures
- **WebcamViewer** - view/test your webcam offline
- **ShareX** - takes screenshots/screencasts, can directly upload images online
- **Oracle VM VirtualBox** - create/host virtual machines
- **WoX** - mac's spotlight for windows
- **Monitorian** - software to control the brightness on your screens/monitors
- **KDE Connect** - easily communicate between devices, like your phone and PC (send files, media controls, run commands, etc.)
- **rclone** - CLI to manage files in cloud storage, can compare directories on a local filesystem to directories in cloud storage
